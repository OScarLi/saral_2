require "saral/version"
require "saral/routing"
require 'saral/controller'

module Saral

  class Application

    def call(env)
      begin
        klass, action = get_controller_and_action(env)
        action = action.to_s
        controller = klass.new(env)
        text = controller.send(action)
        [200, {'Content-Type' => 'text/html'}, [text ? text : 'No Such Action']]
      end
    end

  end

end
