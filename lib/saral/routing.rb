# saral/lib/saral/routing.rb
module Saral
  class Application
    def get_controller_and_action(env)
      begin
      _, cont, action, * = env["PATH_INFO"].split('/')
      cont = cont.to_s.capitalize + "Controller"
      [Object.const_get(cont), action.to_s]
      end
    end
  end
end
