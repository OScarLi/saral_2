module Saral

  class Controller
    attr_reader :env

    def initialize(env)
      @env = env
    end

    def method_missing(meth, *args, &blk)
      "#{meth} route not found: 404"
    end
  end

end
